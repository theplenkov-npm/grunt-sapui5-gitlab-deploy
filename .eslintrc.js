module.exports = {
  env: {
    es6: true,
    node: true
  },
  globals: {
    sap: true,
    $: true,
    window: true,
    jQuery: true,
    fetch: true,
    document: true
  },
  extends: "eslint:recommended",
  parserOptions: {
    ecmaVersion: 2018
  },
  plugins: ["prettier"],
  rules: {
    "linebreak-style": ["error", "windows"],
    quotes: ["error", "double"],
    semi: ["error", "always"],
    "no-unused-vars": ["error", { args: "none" }],
    "getter-return": "off",
    "no-use-before-define": ["error", { functions: false }],
    "consistent-return": "off",
    "prettier/prettier": "error"
  }
};
